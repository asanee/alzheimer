package asanee.tosaganjana.kku.ac.th.alzheimer.outside;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import asanee.tosaganjana.kku.ac.th.alzheimer.firebase.Firebase;

import static asanee.tosaganjana.kku.ac.th.alzheimer.service.NotificationStatus.isNotificationOutside;

import asanee.tosaganjana.kku.ac.th.alzheimer.R;
import asanee.tosaganjana.kku.ac.th.alzheimer.main.MainFragment;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class OutsideFragment extends Fragment {

    View view;
    DatabaseReference myRef;
    String outSide;
    private ProgressDialog mProgress;

    public OutsideFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mProgress = new ProgressDialog(getActivity());
        mProgress.setMessage("Please wait...");
        mProgress.setCancelable(false);
        mProgress.setIndeterminate(true);

        String UID = FirebaseAuth.getInstance().getCurrentUser().getUid();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        myRef = database.getReference(UID).child("outside");

        view = inflater.inflate(R.layout.fragment_outside, container, false);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String leave = dataSnapshot.child("leave").getValue(String.class);
                String address = dataSnapshot.child("address").getValue(String.class);
                String time = dataSnapshot.child("time").getValue(String.class);

                onStayHome(leave, address, time);

                getActivity().setTitle("สถานะ".toUpperCase());
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        return view;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);


    }

    private void onStayHome(String stayHome, String address, String time) {

        RelativeLayout bg = (RelativeLayout) view.findViewById(R.id.bg_outside);
        RelativeLayout layoutStay = (RelativeLayout) view.findViewById(R.id.status_outside_stay);
        RelativeLayout layoutLeave = (RelativeLayout) view.findViewById(R.id.status_outside_leave);

        TextView tvAddress = (TextView) view.findViewById(R.id.leave_address_outside);
        TextView tvTime = (TextView) view.findViewById(R.id.leave_time_outside);

        final ImageButton btnFound = (ImageButton) view.findViewById(R.id.btn_leave);
        final ImageButton btnStay = (ImageButton) view.findViewById(R.id.btn_stay);
        final Button btnHistory = (Button) view.findViewById(R.id.btn_history);

        switch (stayHome) {
            //Leave
            case "1":
//                Drawable gradient = getResources().getDrawable( R.drawable.gradient_red);
//                ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(gradient);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(new
                        ColorDrawable(getResources().getColor(R.color.colorRed)));

                layoutStay.setVisibility(View.GONE);
                layoutLeave.setVisibility(View.VISIBLE);

                tvAddress.setText(address);
                tvTime.setText(time);
                bg.setBackgroundResource(R.drawable.gradient_red);
                mProgress.dismiss();
                btnFound.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (v == btnFound) {

                            mProgress.show();



                            AlertDialog.Builder builder =
                                    new AlertDialog.Builder(getActivity());
                            builder.setMessage("พบผู้ป่วยแล้ว?");
                            builder.setPositiveButton("ใช่", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Toast.makeText(getActivity(),
                                            "ใช่", Toast.LENGTH_SHORT).show();
                                    myRef.child("leave").setValue("0");
                                    isNotificationOutside = true;
                                }
                            });
                            builder.setNegativeButton("ไม่", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //dialog.dismiss();
                                }
                            });
                            builder.show();


                        }
                    }
                });

                break;

            // Stay at home
            case "0":
                ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(new
                        ColorDrawable(getResources().getColor(R.color.colorGreen)));
                layoutStay.setVisibility(View.VISIBLE);
                layoutLeave.setVisibility(View.GONE);

                bg.setBackgroundResource(R.drawable.gradient);
                mProgress.dismiss();
                btnStay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (v == btnStay) {

                            myRef.child("leave").setValue("1");

                        }
                    }
                });
                btnHistory.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (v == btnHistory) {

                            FragmentManager manager = getActivity().getSupportFragmentManager();
                            OutsideHistoryFragment outsideHistoryFragment = new OutsideHistoryFragment();
                            manager.beginTransaction().replace(
                                    R.id.content_main_fragment,
                                    outsideHistoryFragment,
                                    outsideHistoryFragment.getTag()
                            ).commit();
                        }
                    }
                });
                break;
            default:

                break;
        }
    }

}
