package asanee.tosaganjana.kku.ac.th.alzheimer.medicine.index;

/**
 * Created by Puppymind on 29/4/2560.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import asanee.tosaganjana.kku.ac.th.alzheimer.R;

public class IndexAdapter extends RecyclerView.Adapter<IndexAdapter.ViewHolder> {

    private List<index> mIndices;
    private Context mContext;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mName;
        public TextView mDoseWeight;
        public TextView mDoseTime;
        public TextView mTime;
        public ImageView mImage;

        public ViewHolder(View view) {
            super(view);

            mName = (TextView) view.findViewById(R.id.medicine_name);
            mDoseWeight = (TextView) view.findViewById(R.id.medicine_dos_weight);
            mDoseTime = (TextView) view.findViewById(R.id.medicine_dos_time);
            mTime = (TextView) view.findViewById(R.id.medicine_time);
            mImage = (ImageView) view.findViewById(R.id.img_active);
        }
    }

    public IndexAdapter(Context context, List<index> dataset) {
        mIndices = dataset;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.recycler_view_row, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        index index = mIndices.get(position);

        viewHolder.mName.setText(index.getName());
        viewHolder.mDoseWeight.setText(index.getDoseWeight() + " เม็ด");
        viewHolder.mDoseTime.setText(index.getDoseTime() + " ครั้ง  |  " + index.getBeforeAfter());
        viewHolder.mTime.setText(index.getTime());
        viewHolder.mImage.setImageResource(imageActive(index.getActive()));

    }

    @Override
    public int getItemCount() {
        return mIndices.size();
    }

    private int imageActive(String active) {

        int id = 0;
        if (active.equals("breakfast")) {
            id = R.drawable.morning;
        } else if (active.equals("lunch")) {
            id = R.drawable.afternoon;
        } else if (active.equals("dinner")) {
            id = R.drawable.evening;
        }
        return id;
    }
}