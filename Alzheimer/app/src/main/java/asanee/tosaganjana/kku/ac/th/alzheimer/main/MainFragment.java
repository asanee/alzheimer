package asanee.tosaganjana.kku.ac.th.alzheimer.main;


import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import asanee.tosaganjana.kku.ac.th.alzheimer.R;
import asanee.tosaganjana.kku.ac.th.alzheimer.medicine.MedicineFragment;
import asanee.tosaganjana.kku.ac.th.alzheimer.outside.OutsideFragment;
import asanee.tosaganjana.kku.ac.th.alzheimer.personal.PersonalFragment;
import asanee.tosaganjana.kku.ac.th.alzheimer.queue.QueueFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment implements View.OnClickListener {


    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        ImageView btnPersonal = (ImageView) view.findViewById(R.id.btn_personal);
        ImageView btnQueue = (ImageView) view.findViewById(R.id.btn_queue);
        ImageView btnOutside = (ImageView) view.findViewById(R.id.btn_outside);
        ImageView btnMedicine = (ImageView) view.findViewById(R.id.btn_medicine);

        btnPersonal.setOnClickListener(this);
        btnQueue.setOnClickListener(this);
        btnOutside.setOnClickListener(this);
        btnMedicine.setOnClickListener(this);

//        getActivity().setTitle("Medicine".toUpperCase());
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();


        return view;
    }

    @Override
    public void onClick(View v) {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        switch (v.getId()) {
            case R.id.btn_medicine:
//                MedicineFragment medicineFragment = new MedicineFragment();
//                manager.beginTransaction().replace(
//                        R.id.content_main_fragment,
//                        medicineFragment,
//                        medicineFragment.getTag()
//                ).commit();
                Intent intent = new Intent(getActivity(),MedicineFragment.class);
                startActivity(intent);

                break;


            case R.id.btn_outside:
                OutsideFragment outsideFragment = new OutsideFragment();
                manager.beginTransaction().replace(
                        R.id.content_main_fragment,
                        outsideFragment,
                        outsideFragment.getTag()
                ).commit();
                break;
            case R.id.btn_queue:
                QueueFragment queueFragment = new QueueFragment();
                manager.beginTransaction().replace(
                        R.id.content_main_fragment,
                        queueFragment,
                        queueFragment.getTag()
                ).commit();
                break;
            case R.id.btn_personal:
                PersonalFragment personalFragment = new PersonalFragment();
                manager.beginTransaction().replace(
                        R.id.content_main_fragment,
                        personalFragment,
                        personalFragment.getTag()
                ).commit();
                break;
        }
    }
}
