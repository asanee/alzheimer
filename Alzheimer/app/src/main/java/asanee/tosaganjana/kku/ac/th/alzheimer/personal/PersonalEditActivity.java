package asanee.tosaganjana.kku.ac.th.alzheimer.personal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import asanee.tosaganjana.kku.ac.th.alzheimer.R;
import asanee.tosaganjana.kku.ac.th.alzheimer.firebase.Firebase;
import asanee.tosaganjana.kku.ac.th.alzheimer.main.MainActivity;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.ContentValues.TAG;
import static asanee.tosaganjana.kku.ac.th.alzheimer.firebase.Firebase.Uid;
import static asanee.tosaganjana.kku.ac.th.alzheimer.main.MainActivity.mStorageRef;
import static asanee.tosaganjana.kku.ac.th.alzheimer.personal.personal.setAddress;
import static asanee.tosaganjana.kku.ac.th.alzheimer.personal.personal.setDate;
import static asanee.tosaganjana.kku.ac.th.alzheimer.personal.personal.setID;
import static asanee.tosaganjana.kku.ac.th.alzheimer.personal.personal.setName;
import static asanee.tosaganjana.kku.ac.th.alzheimer.personal.personal.setTel;
import static asanee.tosaganjana.kku.ac.th.alzheimer.service.NotificationStatus.*;

public class PersonalEditActivity extends AppCompatActivity {

    private static final int PICK_PHOTO_FOR_AVATAR = 1000;
    private StorageReference imageRef;
    private CircleImageView imageView;
    private EditText edtID, edtName, edtDate, edtAddress, edtTel;
    private DatabaseReference myRef;
    public static View viewEdit;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.personal_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

//        if (id == android.R.id.home) {
//            Intent intent = new Intent(this, MainActivity.class);
//            intent.putExtra("message", "personal");
//            isSetting = true;
//            startActivity(intent);
//        } else if (id == R.id.fragment_personal_edit_done) {
//            setEditDone();
//            Intent intent = new Intent(this, MainActivity.class);
//            startActivity(intent);
//        }


        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_edit);

        LayoutInflater layoutInflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        viewEdit = layoutInflater.inflate(R.layout.activity_personal_edit, null );

        getSupportActionBar().setTitle(getString(R.string.text_profile_edit).toUpperCase());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(Firebase.Uid).child("personal");

        edtID = (EditText) findViewById(R.id.edt_id);
        edtName = (EditText) findViewById(R.id.edt_name);
        edtDate = (EditText) findViewById(R.id.edt_date);
        edtAddress = (EditText) findViewById(R.id.edt_address);
        edtTel = (EditText) findViewById(R.id.edt_tel);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String name = dataSnapshot.child("name").getValue(String.class);
                String address = dataSnapshot.child("address").getValue(String.class);
                String birth = dataSnapshot.child("birth").getValue(String.class);
                String tel = dataSnapshot.child("tel").getValue(String.class);
                String id = dataSnapshot.child("id").getValue(String.class);

                edtID.setHint(id);
                edtName.setHint(name);
                edtDate.setHint(birth);
                edtAddress.setHint(address);
                edtTel.setHint(tel);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });


        imageRef = mStorageRef.child(Uid + "/profile.PNG");

        imageView = (CircleImageView) findViewById(R.id.profile_image_edit);

        downloadDataViaUrl();

        final Button btnChangeImage = (Button) findViewById(R.id.changeImage);
        btnChangeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v == btnChangeImage) {
                    pickImage();
                }
            }
        });

        final ImageButton btnDate = (ImageButton) findViewById(R.id.personal_edit_date);
        btnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getSupportFragmentManager(), "datePicker");
            }
        });

    }

    public void pickImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_PHOTO_FOR_AVATAR);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_PHOTO_FOR_AVATAR && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                //Display an error
                return;
            }

            try {

                InputStream inputStream = getContentResolver().openInputStream(data.getData());
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

                Drawable drawable = new BitmapDrawable(getResources(), bitmap);
                Log.d(TAG, String.valueOf(inputStream));

                imageView.setImageDrawable(drawable);


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }
    }

    public void setEditDone() {

        if (!edtID.getText().toString().equals("")) {
            setID = edtID.getText().toString();
        }
        if (!edtName.getText().toString().equals("")) {
            setName = edtName.getText().toString();
        }
        if (!edtAddress.getText().toString().equals("")) {
            setAddress = edtAddress.getText().toString();
        }
        if (!edtTel.getText().toString().equals("")) {
            setTel = edtTel.getText().toString();
        }

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        myRef = database.getReference(Uid).child("personal");

        myRef.child("name").setValue(setName);
        myRef.child("id").setValue(setID);
        myRef.child("address").setValue(setAddress);
        myRef.child("birth").setValue(setDate);
        myRef.child("tel").setValue(setTel);

        imageView.buildDrawingCache();
        Bitmap bitmap = imageView.getDrawingCache();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] task = baos.toByteArray();

        UploadTask mUploadTask = imageRef.putBytes(task);

        imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                myRef.child("image").setValue(uri.toString());
            }
        });

    }

    private void downloadDataViaUrl() {

        imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {

                try {
                    Glide.with(PersonalEditActivity.this)
                            .load(uri)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(imageView);

                } catch (Exception e) {
                    Log.d(TAG, "Error");
                }
                Log.d(TAG, String.valueOf(uri));
            }
        });
    }
}
