package asanee.tosaganjana.kku.ac.th.alzheimer.service;

/**
 * Created by Puppymind on 19/4/2560.
 */

public class NotificationStatus {

    public static boolean isBreakfast = false;
    public static boolean isLunch = false;
    public static boolean isDinner = false;

    public static boolean isNotificationMassage = false;
    public static boolean isNotificationOutside = true;
    public static boolean isNotificationMedicine = true;
    public static boolean isNotificationQueue = true;

    public static boolean isSetting;
}
