package asanee.tosaganjana.kku.ac.th.alzheimer.medicine;

        import android.app.Dialog;
        import android.app.TimePickerDialog;
        import android.os.Bundle;
        import android.preference.Preference;
        import android.support.v4.app.DialogFragment;
        import android.support.v4.app.Fragment;
        import android.text.format.DateFormat;
        import android.util.Log;
        import android.widget.TimePicker;

        import com.google.firebase.database.DatabaseReference;
        import com.google.firebase.database.FirebaseDatabase;

        import java.util.Calendar;

        import asanee.tosaganjana.kku.ac.th.alzheimer.firebase.Firebase;

/**
 * Created by Puppymind on 20/4/2560.
 */

public class TimePickerFragment extends DialogFragment
        implements TimePickerDialog.OnTimeSetListener {

    public static String setTime;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hour, minute,
                DateFormat.is24HourFormat(getActivity()));
    }

    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        // Do something with the time chosen by the user
        try {
            setTime = hourOfDay + "." + minute;

            Fragment prevBreakfast = getFragmentManager().findFragmentByTag("breakfast");
            Fragment prevLunch = getFragmentManager().findFragmentByTag("lunch");
            Fragment prevDinner = getFragmentManager().findFragmentByTag("dinner");

            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference myRef = database.getReference(Firebase.Uid).child("medicine").child("time");

            if (prevBreakfast != null) {
                myRef.child("breakfast").setValue(setTime);
            } else if (prevLunch != null) {
                myRef.child("lunch").setValue(setTime);
            } else if (prevDinner != null) {
                myRef.child("dinner").setValue(setTime);
            }

            Log.d("SetTime", setTime);
        } catch (Exception e) {
            Log.d("error", e.toString());
        }
    }
}
