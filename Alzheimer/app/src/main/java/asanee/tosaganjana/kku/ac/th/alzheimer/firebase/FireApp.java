package asanee.tosaganjana.kku.ac.th.alzheimer.firebase;

import android.app.Application;

import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Puppymind on 26/3/2560.
 */

public class FireApp extends Application {

    @Override
    public void onCreate(){
        super.onCreate();

        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }

}

