package asanee.tosaganjana.kku.ac.th.alzheimer.outside.index;

/**
 * Created by Puppymind on 29/4/2560.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import asanee.tosaganjana.kku.ac.th.alzheimer.R;

public class IndexAdapter extends RecyclerView.Adapter<IndexAdapter.ViewHolder> {

    private List<index> mIndices;
    private Context mContext;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mAddress;
        public TextView mTime;
        public TextView mDate;


        public ViewHolder(View view) {
            super(view);

            mAddress = (TextView) view.findViewById(R.id.history_address);
            mTime = (TextView) view.findViewById(R.id.history_time);
            mDate = (TextView) view.findViewById(R.id.history_date);
        }
    }

    public IndexAdapter(Context context, List<index> dataset) {
        mIndices = dataset;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.recycler_view_history, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        index index = mIndices.get(position);

        viewHolder.mAddress.setText(index.getAddress());
        viewHolder.mTime.setText(index.getTime());
        viewHolder.mDate.setText(index.getDate());


    }

    @Override
    public int getItemCount() {
        return mIndices.size();
    }

}