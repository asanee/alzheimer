package asanee.tosaganjana.kku.ac.th.alzheimer.medicine;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import asanee.tosaganjana.kku.ac.th.alzheimer.R;
import asanee.tosaganjana.kku.ac.th.alzheimer.firebase.Firebase;
import asanee.tosaganjana.kku.ac.th.alzheimer.main.MainActivity;

import static android.content.ContentValues.TAG;
import static asanee.tosaganjana.kku.ac.th.alzheimer.service.NotificationStatus.*;

/**
 * Created by Puppymind on 20/4/2560.
 */
public class SettingsActivity extends AppCompatActivity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle("Setting");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getFragmentManager().beginTransaction().replace(android.R.id.content,
                new SettingFragment()).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra("message", "medicine");
            isSetting = true;
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }


    public class SettingFragment extends PreferenceFragment
            implements Preference.OnPreferenceClickListener {

        // Preferences
        private Preference mPrefBreakfast;
        private Preference mPrefLunch;
        private Preference mPrefDinner;
        //public static final String KEY_PREF_SYNC_CONN = "pref_breakfast";

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences);

            FirebaseDatabase database = FirebaseDatabase.getInstance();
            String UID = FirebaseAuth.getInstance().getCurrentUser().getUid();
            DatabaseReference myRef = database.getReference(UID).child("medicine").child("time");

            setTitle(getString(R.string.text_setting));
            mPrefBreakfast = (Preference) findPreference("pref_breakfast");
            mPrefBreakfast.setOnPreferenceClickListener(this);

            mPrefLunch = (Preference) findPreference("pref_lunch");
            mPrefLunch.setOnPreferenceClickListener(this);

            mPrefDinner = (Preference) findPreference("pref_dinner");
            mPrefDinner.setOnPreferenceClickListener(this);

            myRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    String breakfast = dataSnapshot.child("breakfast").getValue(String.class);
                    String lunch = dataSnapshot.child("lunch").getValue(String.class);
                    String dinner = dataSnapshot.child("dinner").getValue(String.class);

                    mPrefBreakfast.setSummary(breakfast);
                    mPrefLunch.setSummary(lunch);
                    mPrefDinner.setSummary(dinner);
                }

                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value
                    Log.w(TAG, "Failed to read value.", error.toException());
                }
            });

        }

        @Override
        public boolean onPreferenceClick(Preference preference) {
            String dialog = null;
            if (preference == mPrefBreakfast) {
                dialog = "breakfast";
            } else if (preference == mPrefLunch) {
                dialog = "lunch";
            } else if (preference == mPrefDinner) {
                dialog = "dinner";
            }

            DialogFragment newFragment = new TimePickerFragment();
            newFragment.show(getSupportFragmentManager(), dialog);


            return false;
        }
    }
}